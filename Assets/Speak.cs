﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;

public static class Speaker
{
    public static void Speak(this string text)
    {
        var thread = new Thread(delegate ()
        {
            Command(text);
        });

        thread.Start();
    }

    public static Thread SpeakOnLoop(this string text)
    {
        if (text.Length == 0) return null;

        var thread = new Thread(delegate ()
        {
            for (;;) Command(text);
        });

        thread.Start();
        return thread;
    }


    public static void Command(string text)
    {
        var processInfo = new ProcessStartInfo("nircmd.exe", "speak text \"" + text + "\"");
        processInfo.CreateNoWindow = true;
        processInfo.UseShellExecute = false;

        var process = Process.Start(processInfo);

        process.WaitForExit();
        process.Close();
    }
}
