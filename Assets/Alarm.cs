using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Threading;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Alarm : MonoBehaviour
{
    public string WhatToYellAtYou = "The time is {0} . Lets Feed The Baby!";

    public float hours = 8.0f;
    public Text t;
    public Alarm Snoozer = null;
    public Color TextColor = Color.red;
    public bool StartByDefault = true;
    public AudioSource Klaxon = null;

    int count = 1000;
    DateTime alarm;
    Thread current = null;
    UnityAction<Scene, Scene> onDestroy;


    public void Enable()
    {
        t.color = TextColor;
        alarm = DateTime.Now.AddHours(hours);
        count = 10000;
        StartByDefault = true;

        if (current != null && current.IsAlive) current.Abort();
    }

    public void Disable()
    {
        StartByDefault = false;
    }

    public void Silence()
    {
        if (current != null && current.IsAlive) current.Abort();

        if (Klaxon != null) Klaxon.Stop();
    }

    private void Start()
    {
        Application.targetFrameRate = 5;
        QualitySettings.vSyncCount = 2;

        alarm = DateTime.Now.AddHours(hours);
        count = 10000;

        onDestroy += Destroy;
        SceneManager.activeSceneChanged += onDestroy;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Silence();
        }
        if (StartByDefault)
        {
            // Inputs are disabled in any frame where something else has already happened
            if (count < 100)
            {
                if (Input.GetKeyDown(KeyCode.LeftBracket))
                {
                    hours -= 0.5f;
                    Enable();
                }
                if (Input.GetKeyDown(KeyCode.RightBracket))
                {
                    hours += 0.5f;
                    Enable();
                }
                if (Input.GetKeyDown(KeyCode.H))
                {
                    alarm = alarm.AddHours(0.25);
                    count = 10000;
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    alarm = alarm.AddHours(-0.25);
                    count = 10000;
                }
                if (Input.GetKey(KeyCode.Space))
                {
                    alarm = DateTime.Now.AddHours(hours);
                    count = 10000;
                }
            }

            count++;
            if (count > 29)
            {
                var m = (alarm - DateTime.Now).TotalMinutes;

                t.text = "";

                if (m > 60)
                {
                    t.text += (int)(m / 60) + ":";
                }

                t.text += (int)(m % 60) + ":";

                t.text += (int)((alarm - DateTime.Now).TotalSeconds % 60);

                count = 0;
            }

            if (alarm.Ticks < DateTime.Now.Ticks)
            {
                if (current != null && current.IsAlive)
                {
                    current.Abort();
                }

                current = string.Format(WhatToYellAtYou, DateTime.Now.ToString("h:mm tt")).SpeakOnLoop();

                if (Klaxon != null) Klaxon.Play();

                if (Snoozer != null)
                {
                    Disable();
                    Snoozer.Enable();
                }

            }
        }
    }

    void Destroy(Scene a, Scene b)
    {
        Silence();
    }

    private void OnDestroy()
    {
        Silence();

    }
}
